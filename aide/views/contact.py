from django.shortcuts import render
from smtplib import SMTPException
from bs4 import BeautifulSoup as Bs
from django.core.mail import get_connection, send_mail
from django.core.mail.message import EmailMultiAlternatives
from django.conf import settings
import logging
import random

from ..forms import DemandeForm
from core.models import User
from referents.models import Referent

mail_logger = logging.getLogger('smtp')


def demande(request):
    """
    Formulaire de contact pour une demande
    :param request:
    :return:
    """
    if request.method == "POST":
        form = DemandeForm(request.POST)
        if form.is_valid():
            demande = form.save(commit=False)
            if not request.user.is_anonymous:
                demande.user = request.user
            demande.save()

            destinataires = Referent.objects.filter(user__default_instance__departement=demande.departement)
            if not destinataires:
                destinataires = Referent.objects.filter(user__default_instance__departement__isnull=True)
            if not destinataires:
                destinataires = User.objects.filter(groups__name__contains="Administrateurs techniques")

            body_text = "Ceci est une demande émise avec le formulaire de contact de la plateforme Eco-manifestation." + chr(10) + \
                        "--------------------------------------------------------------------------------------------" + chr(10) + \
                        demande.contenu
            body_text = Bs(body_text, "html.parser").get_text()
            body_html = "Ceci est une demande émise avec le formulaire de contact de la plateforme Eco-manifestation.<hr>" + chr(10) + demande.contenu

            mail = EmailMultiAlternatives(subject='[Plateforme Eco-manifestation ' + demande.departement.name + '] ' + demande.get_type_display(),
                                          body=body_text,
                                          from_email=demande.email,
                                          reply_to=[demande.email],
                                          to=[d.user.email for d in destinataires],
                                          connection=get_connection(),
                                          )
            mail.attach_alternative(body_html, 'text/html')
            try:
                mail.send()
            except SMTPException as e:
                if hasattr(e, 'smtp_error'):
                    if type(e.smtp_error) is bytes:
                        msg = e.smtp_error.decode('utf8')
                    else:
                        msg = e.smtp_error
                else:
                    msg = '-'
                mail_logger.exception("aide.contact.demande: erreur smtp , " + str(type(e)) + ", message : " + msg)
                if settings.ADMINS:
                    try:
                        send_mail(subject="SMTPException sur aide/demande",
                                  message="message d'erreur : " + msg,
                                  from_email=settings.DEFAULT_FROM_EMAIL,
                                  recipient_list=[admin[1] for admin in settings.ADMINS],
                                  connection=get_connection())
                    except SMTPException:
                        pass

            return render(request, "aide/contact-reponse.html")
    else:
        if not request.user.is_anonymous and hasattr(request.user, 'email'):
            code = str(random.randint(1000, 9999))
            form = DemandeForm(initial={'email': request.user.email, 'code': code, 'contenu': code + ' - '})
        else:
            form = DemandeForm()
    return render(request, "aide/contact-form.html", {'form': form})
