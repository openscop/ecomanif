# coding: utf-8
from annoying.decorators import render_to
from django.http.response import Http404, HttpResponse
from django.shortcuts import render
from core.util.user import UserHelper
from django.db.models import Q

from aide.models.context import ContextHelp
from aide.models.help import HelpPage
from aide.models.helpaccordion import HelpAccordionPage, HelpAccordionPanel


def check_affichage_elements(query, request):
    # Vérifier les permissions d'accès aux éléments de la page (notes ou panneaux)
    if request.user.is_anonymous:
        # Récupère les éléments qui n'ont pas de role et pas de département
        elements = query.filter(active=True).filter(role="").filter(departements__isnull=True)

    else:
        user_roles = UserHelper.get_role_names(request.user)
        # Renvoie chaine "national" pour les utilisateurs d'instance nationale ce qui revient à bloquer les éléments qui ont un département assigné
        user_departement = request.user.get_instance().get_departement() != None and request.user.get_instance().get_departement().name or "national"

        # Récupère les éléments qui ont un département indéfinie ou un département conforme à l'utilisateur
        elements = query.filter(active=True).filter(
                Q(departements__isnull=True) | Q(departements__name__contains=user_departement))

        # Retourne True si pas de role ou que le (ou les) roles de l'élément sont en intersection avec le (ou les) role de l'utilisateur
        def check_role(user_roles, element):
            if 'organisateur' in element.role and 'organisateur' in user_roles:
                if not request.user.organisateur.est_diplome():
                    return False
            return (element.role == "" or bool(
                set(user_roles).intersection(set([name for name in element.role.split(',') if name]))))

        # Ne conserver les éléments que si role de l'élément indéfinie ou role de l'élément conforme aux roles de l'utilisateur
        elements = [element for element in elements if check_role(user_roles, element)]
    return elements


def view_help_page(request, path, pk=None):
    """ Trouver une page d'aide 'classique' ou accordion, ou lever un HTTP404 """
    # Pour les pages indexées sur le département avec un utilisateur sans département
    if 'NATIONALE' in path:
        return render(request, "core/notification.html",
                      {'title': "Page de département",
                       'message': "La page demandée dépends du département, vous n'êtes pas relié à un département !",
                       'type': 1,
                       'suite': 'home_page'})

    try:
        page = HelpPage.objects.get(path=path)
        accordion = False
    except HelpPage.DoesNotExist:
        try:
            page = HelpAccordionPage.objects.get(path=path)
            accordion = True
        except HelpAccordionPage.DoesNotExist:
            return render(request, "core/notification.html",
                          {'title': "Page non trouvée", 'message': "La page demandée n'a pas été créée !", 'type': 1,
                           'suite': 'evenements:tableau-de-bord-organisateur'})

    # Si get_for_url n'a pas renvoyé de 404, effectuer le rendu

    # Vérifier les permissions d'accès à la page
    if page.active is False:
        return render(request, "core/notification.html", {'title': "Accès aux pages", 'type': 1,
                                                          'message': "La page n'est plus active"})
    if request is not None:
        # Recherche des rôles et départements sélectionnés pour la page
        help_roles = [name for name in page.role.split(',') if name]
        departements = [dept.name for dept in page.departements.all()]
        # Pas d'accès aux visiteurs dans les cas suivants
        if request.user.is_anonymous:
            if page.authenticated_only or help_roles or departements:
                return render(request, "core/notification.html",
                              {'title': "Accès restreint", 'type': 1,
                               'message': "Vous devez être connecté pour avoir accès à cette page"})
        else:
            if 'organisateur' in help_roles and 'Organisateur' in request.user.get_friendly_role_names():
                if not request.user.organisateur.est_diplome():
                    return render(request, "core/notification.html",
                                  {'title': "Accès restreint", 'type': 1,
                                   'message': "Cette page concerne les organisateurs diplômés"})
            if not 'Editeur' in request.user.get_friendly_role_names():
                user_roles = UserHelper.get_role_names(request.user)
                user_departement = request.user.get_instance().get_departement_name()
                # Si le contenu cible des rôles particuliers, filtrer l'accès
                if help_roles:
                    if not bool(set(user_roles).intersection(set(help_roles))):
                        return render(request, "core/notification.html",
                                      {'title': "Accès restreint", 'type': 1,
                                       'message': "Cette page ne vous est pas destinée"})
                # Si le contenu cible des départements particuliers, filtrer l'accès
                if departements:
                    if user_departement not in departements:
                        return render(request, "core/notification.html",
                                      {'title': "Accès restreint", 'type': 1,
                                       'message': "Cette page ne concerne pas votre département"})

    # S'il s'agit d'une page de type accordion
    if accordion:
        if pk:
            pk = int(pk)
        try:
            activ = page.get_tabs()[0].title
            if pk:
                activ = HelpAccordionPanel.objects.filter(pk=pk).first().tab.title
        except:
            activ = None
        panels = check_affichage_elements(page.get_panels(), request)
        # tabs = page.get_tabs().values_list('title', flat=True)
        # Générer la liste des onglets en supprimant ceux qui n'ont pas de panneaux visibles
        tabs = []
        for tab in page.get_tabs():
            for panel in tab.panels.all():
                if panel in panels:
                    tabs.append(tab.title)
                    break

        template_path = 'aide/help-page-accordion.html'
        response = render(request, template_path, {'page': page,
                                                   'panels': panels,
                                                   'groups': tabs,
                                                   'count': len(tabs),
                                                   'activ_tab': activ,
                                                   'focus': pk})
    # S'il s'agit d'une 'simple' page
    else:
        if not request.user.is_anonymous and 'Organisateur' not in request.user.get_friendly_role_names():
            notes = page.notes.all()
        else:
            notes = check_affichage_elements(page.notes, request)
        if request.GET.get('embed'):
            template_path = 'aide/help-texte.html'
        else:
            template_path = 'aide/help-page.html'
        response = render(request, template_path, {'page': page, 'notes': notes})

    return response


@render_to('aide/help-page-list.html')
def view_help_list(request, *args, **kargs):
    """ Afficher la liste des pages d'aide """
    if 'Editeur' in request.user.get_friendly_role_names():
        helppages = HelpPage.objects.all().order_by('title')
        helpaccordionpages = HelpAccordionPage.objects.all().order_by('title')
        return {'helppages': helppages, 'helpaccordionpages': helpaccordionpages}
    else:
        return {}


def view_context_help(request, name):
    """ Contenu des aides contextuelles, à récupérer en AJAX """
    ctx = ContextHelp.objects.for_identifier(name)
    if ctx.exists():
        contents = "<br>".join(ctx.values_list('text', flat=True))
        response = HttpResponse(contents)
        response['X-Robots-Tag'] = 'noindex'
        return response
    raise Http404()
