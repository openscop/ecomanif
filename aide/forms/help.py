# coding: utf-8
from extra_views import InlineFormSetFactory
from ckeditor.widgets import CKEditorWidget
from ckeditor_uploader.widgets import CKEditorUploadingWidget
from django.forms import models
from django.forms.fields import TypedMultipleChoiceField

from aide.models.help import HelpPage, HelpNote
from core.util.user import UserHelper


class HelpPageForm(models.ModelForm):
    """ Formulaire admin des nouveautés """
    model = HelpPage

    # Champs
    role = TypedMultipleChoiceField(choices=UserHelper.ROLE_CHOICES, required=False, label="Rôle")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if 'role' in self.initial and self.initial['role']:
            self.initial['role'] = self.initial['role'].split(',')

    def save(self, commit=True):
        """Sauvegarde de l'objet"""
        self.instance.role = ','.join(sorted(self.cleaned_data['role']))
        return super().save(commit=commit)

    class Meta:
        exclude = []
        widgets = {'content': CKEditorUploadingWidget()}


class HelpNoteForm(models.ModelForm):
    """ Formulaire admin des nouveautés """
    model = HelpNote

    # Champs
    role = TypedMultipleChoiceField(choices=UserHelper.ROLE_CHOICES, required=False, label="Rôle")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['departements'].help_text = None
        if 'role' in self.initial and self.initial['role']:
            self.initial['role'] = self.initial['role'].split(',')

    def save(self, commit=True):
        """Sauvegarde de l'objet"""
        self.instance.role = ','.join(sorted(self.cleaned_data['role']))
        return super().save(commit=commit)

    class Meta:
        exclude = []
        widgets = {'content': CKEditorWidget('minimal')}


class HelpNoteInLine(InlineFormSetFactory):
    """ Formset InLine des notes de page d'aide """

    model = HelpNote
    fields = ['title', 'active', 'content', 'role', 'departements']
    factory_kwargs = {'extra': 1, 'max_num': None}
    formset_kwargs = {'auto_id': 'my_id_%s'}
    form_class = HelpNoteForm