# coding: utf-8
from django import forms

from ckeditor.widgets import CKEditorWidget
from crispy_forms.layout import Layout, Fieldset, Submit, HTML

from core.forms.base import GenericForm
from aide.models import Demande


class DemandeForm(GenericForm):
    """ Formulaire de contact """

    code = forms.CharField(max_length=4, min_length=4, label='Code antispam',
                           help_text='Entrez un code en quatre chiffres que vous reporterez au début de votre demande juste ci-dessous.')

    def clean(self):
        cleaned_data = super().clean()
        if "code" in cleaned_data and "contenu" in cleaned_data:
            if cleaned_data['code'] not in cleaned_data['contenu']:
                self.add_error('code', "le code arbitraire ne correspond pas !")
        return cleaned_data

    # Overrides
    def __init__(self, *args, **kwargs):
        super(DemandeForm, self).__init__(*args, **kwargs)
        self.helper.layout = Layout(Fieldset(*["Demande", 'type', 'departement', 'code', 'contenu', 'email']),
                                    HTML('<div class="text-center">'),
                                    Submit('submit', 'Envoyer', css_class="btn-half mt-4"),
                                    HTML('</div>'))
        self.helper.field_class = 'col-sm-8'

    class Meta:
        model = Demande
        fields = ["type", "departement", "code", "contenu", "email"]
        widgets = {'contenu': CKEditorWidget(config_name='minimal')}
        help_texts = {'contenu': 'Entrez en début de texte le code choisi dans le champ antispam précédent'}


class DemandeAdminForm(GenericForm):
    """ Gestion des demandes de contact """

    class Meta:
        model = Demande
        exclude = []
        widgets = {'contenu': CKEditorWidget(config_name='minimal')}
