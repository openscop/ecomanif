# Generated by Django 2.1.2 on 2019-02-15 08:00

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('aide', '0002_demande_user'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='helpnote',
            options={'verbose_name': 'Note de page', 'verbose_name_plural': 'Notes de pages'},
        ),
        migrations.AlterModelOptions(
            name='helppage',
            options={'verbose_name': 'Page personnalisée', 'verbose_name_plural': 'Pages personnalisées'},
        ),
    ]
