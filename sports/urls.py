# coding: utf-8
from django.urls import path

from sports.views.ajax import ActiviteAJAXView, get_discipline_id_from_activite_name, get_activite_id_from_name
from sports.views.discipline import DisciplinesActivitesView, DisciplinesFederationsView


app_name = 'sports'
urlpatterns = [

    # Widget AJAX
    path('ajax/activite/', ActiviteAJAXView.as_view(), name='activite_widget'),
    path('ajax/discipline/by_activite_name/', get_discipline_id_from_activite_name, name='discipline_by_activite_name'),
    path('ajax/activite/by_name/', get_activite_id_from_name, name='activite_by_name'),
    path('disciplines/activites/', DisciplinesActivitesView.as_view(), name='disciplines_activites'),
    path('disciplines/federations/', DisciplinesFederationsView.as_view(), name='disciplines_federations'),

]
