from django.views.generic.list import ListView

from sports.models.sport import Discipline
from sports.models.federation import Federation
from administrative_division.models.departement import Departement, LISTE_DEPARTEMENT


class DisciplinesActivitesView(ListView):

    model = Discipline
    template_name = "sports/disciplinesactivites.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context


class DisciplinesFederationsView(ListView):

    model = Discipline
    template_name = "sports/disciplinesfederations.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        liste_dept = []
        for d in Departement.objects.configured():
            liste_dept.append((d.name, d))
        context['liste_dept'] = liste_dept
        dept = self.request.GET.get('dept')
        if dept:
            regional = Federation.objects.none()
            for departement in LISTE_DEPARTEMENT:
                if departement[0] ==dept:
                    regional = Federation.objects.filter(region=departement[2])
            departemental = Federation.objects.filter(departement__name=dept)
            context['liste_federations'] = departemental.union(regional)
        else:
            context['liste_federations'] = Federation.objects.all()
        return context
