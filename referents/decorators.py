# coding: utf-8
from django.shortcuts import get_object_or_404, render
from functools import wraps

from core.util.permissions import require_role
from evenements.models import Manif


def verifier_secteur_instruction(function=None):
    """ Décorateur : limite l'accès des instructions en fonction du secteur """

    def decorator(view_func):
        @require_role(['referent'])
        @wraps(view_func)
        def _wrapped_view(request, *args, **kwargs):
            manif = get_object_or_404(Manif, pk=kwargs['pk'])
            # L'instructeur ne peut accéder qu'aux instructions de son département ou de son arrondisement
            if request.user.get_instance().is_master():
                return view_func(request, *args, **kwargs)
            user_place = request.user.get_instance().get_departement()
            object_place = manif.get_instance().get_departement()
            if user_place == object_place:
                return view_func(request, *args, **kwargs)
            return render(request, "core/notification.html",
                          {'title': "Accès restreint", 'type': 1,
                           'message': "Vous n'avez pas les droits d'accès à cette page"})

        return _wrapped_view

    if function:
        return decorator(function)
    return decorator
