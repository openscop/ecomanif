from django.contrib import admin
from related_admin import RelatedFieldAdmin

from .models import Referent


@admin.register(Referent)
class ReferentAdmin(RelatedFieldAdmin):
    """ Configuration de l'admin Référent """
    list_display = ['pk', 'user']
