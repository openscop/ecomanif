# coding: utf-8
from django.utils.decorators import method_decorator
from django.views.generic import ListView, DetailView
from django.utils import timezone

from core.util.permissions import require_role
from evenements.models import Manif
from .decorators import verifier_secteur_instruction


@method_decorator(require_role('referent'), name='dispatch')
class TableauDeBordReferent(ListView):
    """ Tableau de bord organisateur """

    # Configuration
    model = Manif
    template_name = 'evenements/dashboard.html'

    # Overrides
    def get_queryset(self):
        """ Renvoyer les manifestations à afficher dans le tableau de bord """
        if self.request.method == 'GET' and 'archive' in self.request.GET:
            return Manif.objects.filter(finalise=True).filter(date_fin__lt=timezone.now()).order_by('date_debut')
        manif = Manif.objects.filter(finalise=True).filter(date_fin__gte=timezone.now()).order_by('-date_debut')
        if self.request.user.get_departement():
            return manif.filter(ville_depart__arrondissement__departement=self.request.user.get_departement())
        else:
            return manif

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if 'archive' in self.request.GET:
            context['archive'] = True
        return context


@method_decorator(verifier_secteur_instruction, name='dispatch')
class ManifDetailReferent(DetailView):
    """ Vue de détail d'une manifestation pour un referent """

    model = Manif
    template_name = 'evenements/evenement_detail.html'

    # Overrides
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # Pour les barres de progression
        if hasattr(self.object, 'charte'):
            diplome = self.object.charte.diplome()
            if diplome == "or":
                context['bg_bar_charte'] = "bg-bar-or"
            elif diplome == "argent":
                context['bg_bar_charte'] = "bg-bar-arg"
            elif diplome == "bronze":
                context['bg_bar_charte'] = "bg-bar-bro"
            else:
                context['bg_bar_charte'] = "bg-secondary progress-bar-striped"
            bg_bar_grp = []
            for item in range(0,5):
                niveau = self.object.charte.niveau_grp()[item]
                if niveau == 'oko':
                    bg_bar_grp.append("bg-bar-or")
                elif niveau == 'oka':
                    bg_bar_grp.append("bg-bar-arg")
                elif niveau == 'okb':
                    bg_bar_grp.append("bg-bar-bro")
                else:
                    bg_bar_grp.append("bg-secondary")
            context['bg_bar_grp'] = bg_bar_grp
        return context
