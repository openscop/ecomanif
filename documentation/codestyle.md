Style du code
=====
- Nombre maximum de colonnes : 160
- Indentation de 4 espaces (ASCII 32)
- Commentaires d'une ligne au format "# text"
- Commentaires de fin de ligne précédés de deux espaces
- Docstrings au format triple guillemets anglais (")
- Éléments de docstrings au format ":param name: desc"

Commits
=======
- __[m]__ opérations diverses
- __[t]__ tests (correction ou ajout de tests)
- __[f]__ fix (correction d'un bug ou amélioration d'une fonction)
- __[+]__ ajout de code ou fonctionnalité
- __[-]__ suppression de code ou fonctionnalité
- __[d]__ modification de la documentation

(2017/02/02) Les commits devraient suivre les 7 règles suivantes :

- Séparer le sujet du corps du commit par une ligne vide
- Limiter la longeur du sujet à 64 caractères (50 pour l'anglais)
- Typo correcte dans le sujet, notamment en termes de majuscules
- Ne pas terminer la ligne du sujet par un point
- Décrire le commit en utilisant si possible l'impératif/infinitif
- Chaque ligne du corps ne doit pas excéder 72 caractères
- Utiliser le corps du commit pour expliquer quoi/pourquoi plutôt que comment

Le titre du commit, devrait si possible faire suite à cette introduction :

« Appliquer ce commit va... »
