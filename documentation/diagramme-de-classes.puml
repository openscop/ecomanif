@startuml

Title Classes métiers de la plateforme Manifestation Sportive

namespace evenements {

    class Manif {
        nom CharField
        date_creation : DateTimeField
        date_debut : DateTimeField
        date_fin : DateTimeField
        description : TextField
        nb_participants : PositiveIntegerField
        nb_spectateurs : PositiveIntegerField
        prenom_contact : CharField
        nom_contact : CharField
        tel_contact : CharField
        email_contact : EmailField


        finalise : BooleanField
        prive : BooleanField
        cache : BooleanField
        afficher_adresse_structure : BooleanField

        cartographie : FileField
        convention : FileField
        docs_additionels : FileField
        ..
        -instance : ForeignKey
        -structure : ForeignKey
        -activite : ForeignKey
        -ville_depart : ForeignKey
        -parcs : ManyToManyField
        -villes_traversees : ManyToManyField
        -departements_traverses : ManyToManyField
        ==
        ajouter_document()
        liste_referents()
        formulaire_complet()
        dossier_complet()
        liste_manquants()
        etc...
    }

    class SansInstanceManif {
    }

    class Charte {
        Conservation et gestion durable de la biodiversité et des ressources naturelles
        Consommation et production responsables
        Sensibilisation et éco-communication
        Transport et mobilité durable & energie
        Responsabilité sociale du sport et solidarité
    }
    Manif <|-- SansInstanceManif

    abstract class InstructionConfig {
        LISTE_CHAMPS_ETAPE_0 : Constante
        LISTE_FICHIERS_ETAPE_0 : Constante
        LISTE_FICHIERS_ETAPE_1 : Constante
        LISTE_FICHIERS_ETAPE_2 : Constante

        {abstract} ref_cerfa : CharField
        {abstract} consultFedeRequis : BooleanField
        {abstract} modeConsultServices : SmallIntegerField / choices
        {abstract} delaiDepot : SmallIntegerField
        {abstract} delaiDepot_ndept : SmallIntegerField
        {abstract} delaiDocComplement1 : SmallIntegerField
        {abstract} delaiDocComplement2 : SmallIntegerField
    }
    InstructionConfig <|-- AbstractVtm
    InstructionConfig <|-- AbstractDnm

    note right of InstructionConfig
        modeConsultServices :
        - Sans consultation
        - Consultation optionnel
        - Consultation obligatoire
    end note

    class DocumentComplementaire {
        information_requise : TextField
        document_attache : FileField
        ..
        -manif : ForeignKey
        ==
        __str__()
        log_doc_fourni()
        log_doc_demande()
        notifier_doc_fourni()
        notifier_doc_demande()
        demande_doc()
        doc_fourni()
        get_prefecture()
        existe()
        }
    Manif "1" -- "*" DocumentComplementaire

}

namespace organisateurs {

    class StructureType {
        type_of_structure : CharField
        __str__()
        natural_key()
    }

    class Structure {
        name : CharField
        address : CharField
        phone : CharField
        website : URLField
        ..
        -commune : ForeignKey
        -type_of_structure : ForeignKey
        -organisateur : OneToOne
        ==
        __str__()
        get_absolute_url()
        natural_key()
        get_departement()
    }
    Structure "1" -- "1" Organisateur
    StructureType "1" -- "*" Structure

    class Organisateur {
        cgu : BooleanField
        ..
        -user : ForeignKey
        ==
        __str__()
        save()
        get_departement()
        get_departement_name()
        get_url()
        create_openrunner_user()
    }
}

namespace sports {
    class Federation {
        short_name : CharField
        name : CharField
        email : EmailField
        multisports : BooleanField
        level : SmallIntegerField / choices
        region : CharField
        ..
        -departement : ForeignKey
        -discipline : ManyToMany
        ==
        __str__()
        natural_key()
        save()
        get_agents()
        get_agent_count()
        is_departemental()
        is_regional()
        is_national()
        get_discipline_count()
    }
    Federation "*" -- "*" Discipline

    note left of Federation
        level :
        - Départemental
        - Régional
        - National
    end note

    class Discipline {
        name : CharField
        milieu : SmallIntegerField / choices
        motorise : BooleanField
        __str__()
        natural_key()
        get_federations()
        get_activites()
        get_activite_count()
    }

    note left of Discipline
        milieu :
        - Nautique
        - Terrestre
        - Aérien
    end note

    class Activite {
        name : CharField
        ..
        -discipline : ForeignKey
        ==
        __str__()
        natural_key()
        get_federations()
        get_incoming_manifestation_count()
    }
    Discipline "1" -- "*" Activite
}

namespace core {
    class Instance {
        name : CharField
        or_map_enabled : BooleanField
        or_map_required : BooleanField
        color : CharField
        workflow_ggd : SmallIntegerField / choices
        workflow_ddsp : SmallIntegerField / choices
        workflow_sdis : SmallIntegerField / choices
        workflow_cg : SmallIntegerField / choices
        instruction_mode : SmallIntegerField / choices
        manifestation_delay : SmallIntegerField
        manifestation_anm_1dept_delay : SmallIntegerField
        manifestation_anm_ndept_delay : SmallIntegerField
        manifestation_dvtm_delay : SmallIntegerField
        manifestation_avtmc_delay : SmallIntegerField
        manifestation_avtm_vp_delay : SmallIntegerField
        manifestation_avtm_vn_delay : SmallIntegerField
        manifestation_avtm_homolog_delay : SmallIntegerField
        sender_email : CharField
        email_settings : CharField
        ..
        -departement : OneToOneField
        ==
        is_master()
        get_email()
        uses_openrunner()
        is_openrunner_map_required()
        get_default_color()
        get_workflow_ggd()
        get_workflow_ddsp()
        get_workflow_sdis()
        get_workflow_cg()
        get_instruction_mode()
        get_departement()
        get_departement_name()
        get_departement_friendly_name()
        get_avis_delay()
        get_federation_avis_delay()
        get_manifestation_delay()
        get_manifestation_anm_ldept_delay()
        get_manifestation_anm_ndept_delay()
        get_manifestation_dvtm_delay()
        get_manifestation_avtmc_delay()
        get_manifestation_avtm_vp_delay()
        get_manifestation_avtm_vn_delay()
        get_manifestation_avtm_homolog_delay()
        get_email_settings()
        get_color()
        send_test_mail()
        save()
        __str__()
        natural_key()
    }

    abstract class User {
        status : SmallIntegerField / choices
        ..
        -default_instance : ForeignKey
        ==
        get_instance()
        get_departement()
        get_service()
        get_role()
        has_role()
        get_friendly_role_names()
        get_full_name()
        set_instance()
    }

    note right of User
        status :
        - Normal
        - Nom d'utilisateur modifié
    end note

    class UserHelper {
        get_role_names()
        get_role_name()
        has_role()
        get_service_instance()
    }

    note right of UserHelper
        Agent :
        - ggdagent
        - edsragent
        - brigadeagent
        - codisagent
        - sdisagent
        - cisagent
        - ddspagent
        - serviceagent
        - federationagent
        - mairieagent
        - cgagent
        - cgsuperieuragent'
    end note

    note left of UserHelper
        AgentLocal :
        - cgdagentlocal
        - commissariatagentlocal
        - cgserviceagentlocal
        - edsragentlocal
        - compagnieagentlocal

        Role direct :
        - organisateur
        - instructeur
        - observateur
        - secouriste
    end note
    Instance --[hidden] User
    User .. UserHelper

    note bottom of UserHelper
    Enchainement des objets : User.UserRole.UserService
    Exemple : user.ggdagent.ggd
    end note
}

@enduml