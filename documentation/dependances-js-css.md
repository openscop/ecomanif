Inventaire des dépendances Javascript et CSS
===

!! Travail en cours !!

NB : les numéros de version mentionnés dans ce document peuvent ne pas avoir été actualisé lors des mises à jour.


### Bootstrap 4 Ti-Ta-Toggle

Permet d'embellir les cases à cocher.

Source : http://kleinejan.github.io/titatoggle/#intro

**Remarques**

- Compatible avec Bootstrap 4
- Ajout d'un fichier CSS seulement

**Installation**

- Copie de la librairie dans *portail/static/libs/titatoggle*
- Ajout de la dépendance dans le < head > du template *portail/templates/base.html*

Pour fonctionner, il est nécessaire que la balise < input > soit à l'intérieur de la balise < label > (ce qui est le cas 
avec django-crispy-forms) et qu'une balise <span> soit placée immédiatement après la balise < input >. Il est donc 
nécessaire de modifier le template d'affichage des balises < input > de django-crispy-forms.

- Copier le fichier env/lib/python3.6/site-packages/crispy_forms/templates/bootstrap4/field.html
et le coller dans templates/bootstrap4/field.html
- Ajouter la balise < span >
- Ajouter la classe "checkbox-slider--a" 

### jQuery chosen

Permet d'embellir les boites de sélection des formulaire.

Source : https://harvesthq.github.io/chosen/

**Remarques**

- Plugin de jQuery

**Installation**

- Copie de la librairie dans *portail/static/libs/chosen*
- Ajout des dépendances (css et js) dans le < head > du template *portail/templates/base.html*
- Ajout de la commande d'initialisation dans le script JS *portail/static/portail/js/main.js*

    $('select').chosen();

Cette commande va transformer toutes les balises <select> de la page.
On peut passer, dans le même temps, un dictionnaire pour paramètrer le rendu, 
comme dans le script du fichier *instructions/templates/instructions/instruction_dispatch_form.html*<br>
(commande nécessaire pour l'initialisation des selects de la page insérée)

    $('select').chosen({
        placeholder_text_multiple: 'Laissez vide pour tous',
        no_results_text: "Aucun résultat avec : ",
    });

Pour provoquer la mise à jour d'un champ select-chosen , il faut passer la commande suivante :

$('#mon-select-chosen').trigger("chosen:updated");
