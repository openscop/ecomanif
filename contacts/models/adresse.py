# coding: utf-8
from django.contrib.contenttypes.fields import GenericForeignKey
from django.db import models


class Adresse(models.Model):
    """ Adresse postale """

    # Champs
    address = models.CharField("adresse", max_length=255)
    commune = models.ForeignKey('administrative_division.commune', verbose_name='commune', on_delete=models.CASCADE)
    content_type = models.ForeignKey('contenttypes.contenttype', null=True, on_delete=models.SET_NULL)
    object_id = models.PositiveIntegerField(null=True)
    content_object = GenericForeignKey('content_type', 'object_id')

    # Overrides
    def __str__(self):
        """ Renvoyer la chaîne pour l'adresse """
        return ' - '.join([self.address, ' '.join([self.commune.zip_code, self.commune.name.upper()])])

    def natural_key(self):
        return self.address, self.commune

    def get_departement(self):
        """ Renvoyer le département de l'adresse """
        return self.commune.get_departement()

    # Meta
    class Meta:
        verbose_name = "adresse"
        verbose_name_plural = "adresses"
        app_label = 'contacts'
