# coding: utf-8
import factory

from ..models import Contact


class ContactFactory(factory.django.DjangoModelFactory):
    """ Factory des contacts """

    # Champs
    first_name = 'John'
    last_name = 'Doe'
    phone = '06 55 55 55 55'

    # Meta
    class Meta:
        model = Contact
