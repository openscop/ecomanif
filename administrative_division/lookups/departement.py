# coding: utf-8
from ajax_select import register, LookupChannel

from administrative_division.models.departement import Departement


@register('departement')
class DepartementLookup(LookupChannel):
    """ Lookup AJAX des départements """

    # Configuration
    model = Departement

    # Overrides
    def get_query(self, q, request):
        return self.model.objects.filter(name__icontains=q).order_by('name')[:20]

    def format_item_display(self, item):
        return u"<span class='tag'>%s</span>" % item.name
