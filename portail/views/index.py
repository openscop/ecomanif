# coding: utf-8
import re

from django.views.generic.base import TemplateView
from django.shortcuts import render
from django.contrib.flatpages.models import FlatPage

from core.models.instance import Instance


class HomePage(TemplateView):
    """ Page d'accueil """

    # Configuration
    template_name = 'portail/index.html'

    # Méthodes
    def get_context_data(self, **kwargs):
        """ Définir le contexte d'affichage de la page """
        context = super(HomePage, self).get_context_data(**kwargs)
        instance = Instance.objects.get_for_request(self.request)
        context['INSTANCES'] = Instance.objects.configured()
        # Domaine sans instance : ne pas afficher le calendrier
        context['HIDE_CALENDAR'] = instance.is_master()
        if FlatPage.objects.filter(url="/index/").exists():
            page_index = FlatPage.objects.get(url="/index/")
            context['texte'] = page_index.content
        else:
            context['texte'] = "Page non trouvée dans la base de données"
        if FlatPage.objects.filter(url="/presentation/").exists():
            page_presentation = FlatPage.objects.get(url="/presentation/")
            context['presentation'] = page_presentation.content
        else:
            context['presentation'] = "Page non trouvée dans la base de données"
        if FlatPage.objects.filter(url="/boiteoutils/").exists():
            page_boiteoutils = FlatPage.objects.get(url="/boiteoutils/")
            if "{{" in page_boiteoutils.content:
                liste = re.findall(r'\{\{.+\}\}', page_boiteoutils.content)
                for item in liste:
                    if item == "{{ INSTANCE }}":
                        page_boiteoutils.content = page_boiteoutils.content.replace(item, instance.get_departement_name())
            context['boiteoutils'] = page_boiteoutils.content
        else:
            context['boiteoutils'] = "Page non trouvée dans la base de données"
        return context


def erreur400view(request, exception):
    return render(request,'400.html', {'message': exception}, status=400)


def erreur403view(request, exception):
    return render(request,'403.html', {'message': exception}, status=403)


def erreur404view(request, exception):
    return render(request,'404.html', {'message': exception}, status=404)


def erreur500view(request):
    return render(request,'500.html', status=500)
