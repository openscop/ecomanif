# coding: utf-8
from django.db.models import IntegerField
from django.utils import timezone
from django.views.generic import ListView
from django.db.models import Value

from core.models.instance import Instance
from evenements.models import Manif
from sports.models import Activite


class Calendar(ListView):
    """ Calendrier des manifestations """

    # Configuration
    model = Manif
    template_name = 'portail/calendar.html'

    # Overrdes
    def get_queryset(self):
        now = timezone.now()
        instance = Instance.objects.get_for_request(self.request)
        departement = instance.get_departement()
        display = Manif.objects.filter(date_debut__gt=now, prive=False, cache=False)
        # Afficher les manifs du département, ou aucune sur le domaine master
        if departement is not None:
            display = display.filter(ville_depart__arrondissement__departement=departement)
        else:
            return display.none()
        return display.order_by('date_debut')

    def get_context_data(self, **kwargs):
        now = timezone.now()
        instance = Instance.objects.get_for_request(self.request)
        departement = instance.get_departement()
        context = super(Calendar, self).get_context_data(**kwargs)
        liste = Activite.objects.filter(manifs__date_debut__gt=now, manifs__cache=False, manifs__prive=False,
                                         manifs__ville_depart__arrondissement__departement=departement)
        context['activities'] = liste.order_by('name')
        return context


class FilteredCalendar(Calendar):
    """ Calendrier des manifestations par activité """

    # Overrides
    def get_queryset(self):
        now = timezone.now()
        instance = Instance.objects.get_for_request(self.request)
        departement = instance.get_departement()
        display = Manif.objects.filter(date_debut__gt=now, prive=False, cache=False).filter(activite=self.kwargs['activite'])
        # Afficher les manifs du département, ou aucune sur le domaine master
        if departement is not None:
            display = display.filter(ville_depart__arrondissement__departement=departement)
        else:
            return display.none()
        return display.order_by('date_debut')

    def get_context_data(self, **kwargs):
        context = super(FilteredCalendar, self).get_context_data(**kwargs)
        context['current_activite'] = int(self.kwargs['activite'])
        return context
