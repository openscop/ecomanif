# coding: utf-8
from django.views.generic.base import TemplateView
from django.contrib.sessions.models import Session
from django.db.models import Count
from django.utils import timezone
from core.models import User
from evenements.models import Manif as EvenementManif
from organisateurs.models import Organisateur
from notifications.models import Action, Notification

from collections import OrderedDict

class Stat(TemplateView):
    """ Page de statistique """

    # Configuration
    template_name = 'stat.html'

    def get_current_users(self):
        active_sessions = Session.objects.filter(expire_date__gte=timezone.now())
        user_id_list = []
        for session in active_sessions:
            data = session.get_decoded()
            user_id_list.append(data.get('_auth_user_id', None))
        return User.objects.filter(id__in=user_id_list)

    def count_by_year(self, **kwargs):
        data = None
        if "liste" in kwargs and "critere" in kwargs:
            liste = kwargs.get('liste')
            critere = kwargs.get('critere')
            data = {}
            for y in range(2014, timezone.now().year + 2):
                filter = critere + "__year"
                data[y] = liste.filter(**{filter: y}).count()
        return OrderedDict(sorted(data.items(), key=lambda t: t[0]))

    def get_context_data(self, **kwargs):
        stat = {}
        stat['annees'] = range(2014, timezone.now().year + 2)

        stat['connected_user'] = self.get_current_users().count()

        stat['nb_utilisateur'] = User.objects.count()
        stat['nb_utilisateur_par_annee'] = self.count_by_year(liste=User.objects, critere="date_joined")

        stat['nb_organisateur'] = Organisateur.objects.count()
        stat['nb_organisateur_par_annee'] = self.count_by_year(liste=Organisateur.objects, critere="user__date_joined")

        stat['nb_manif_prevue'] = EvenementManif.objects.count()
        # stat['nb_manif_prevue_par_annee'] = self.count_by_year(liste = EventsManifestation.objects, critere = "begin_date")
        stat['nb_manif_prevue_par_annee'] = self.count_by_year(liste=EvenementManif.objects, critere="date_debut")

        stat['nb_manif_cree'] = EvenementManif.objects.count()
        # stat['nb_manif_cree_par_annee'] = self.count_by_year(liste = EventsManifestation.objects, critere = "creation_date")
        stat['nb_manif_cree_par_annee'] = self.count_by_year(liste=EvenementManif.objects, critere="date_creation")

        # Code commenté car execution très lente
        # stat['nb_manif_validee'] = [m.processing() for m in Manifestation.objects.all()].count(True)
        # data = {}
        # for y in range(2014, timezone.now().year + 2):
        #     data[y] = [m.processing() for m in Manifestation.objects.filter(begin_date__contains=y).all()].count(True)
        # stat['nb_manif_validee_par_annee'] = data

        stat['nb_manif_une_commune'] = EvenementManif.objects.annotate(nbr_cities=Count('villes_traversees')).filter(
            nbr_cities=1).count()
        # stat['nb_manif_une_commune_par_annee'] = self.count_by_year(liste = EventsManifestation.objects.annotate(nbr_cities=Count('crossed_cities')).filter(nbr_cities=1), critere = "begin_date")
        stat['nb_manif_une_commune_par_annee'] = self.count_by_year(
            liste=EvenementManif.objects.annotate(nbr_cities=Count('villes_traversees')).filter(nbr_cities=1),
            critere="date_debut")

        stat['nb_action'] = Action.objects.count()
        stat['nb_action_par_annee'] = self.count_by_year(liste=Action.objects, critere="creation_date")

        stat['nb_notification'] = Notification.objects.count()
        stat['nb_notification_par_annee'] = self.count_by_year(liste=Notification.objects, critere="creation_date")

        context = super().get_context_data(**kwargs)
        context['stat'] = stat
        return context
