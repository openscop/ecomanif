/**
 * Created by david on 19/10/2017.
 */


$(document).ready(function () {

    // bof Tooltip
    $('[data-toggle="tooltip"]').tooltip({'html': true});
    $('select').chosen({width: "100%"});
    $('form').preventDoubleSubmission();
    // eof Tooltip

    // Former les boutons des comptes tiers
    $('.socialaccount_providers .socialaccount_provider.google').html('<img src="/static/portail/img/btn_google_signin_dark_normal_web@2x.png"/>');
    $('.socialaccount_providers .socialaccount_provider.facebook').html('<img src="/static/portail/img/facebook.png"/>');
    // Supprimer les boutons des comptes déjà associés
    if ($('form .socialaccount_provider')) {
        $('form .socialaccount_provider').each(function () {
            var classes = $(this).attr('class').split(' ');
            classes = '.socialaccount_providers .' + classes.join('.');
            $(classes).parent().remove();
        })
    }

    // bof Checkbox décalé avec django2.0
    $('.col--offset-').addClass('col-sm-offset-3');
    // eof Checkbox décalé avec django2.0

    // bof Changer les boutons radio par des switches bootstrap (librairie tierce)
    //$.fn.bootstrapSwitch.defaults.onText = 'OUI';
    //$.fn.bootstrapSwitch.defaults.offText = 'NON';
    //$("[type='checkbox']").bootstrapSwitch();
    //$("[type='checkbox']").bootstrapSwitch('size', 'mini');
    // eof Changer les boutons radio par des switches bootstrap

    // bof bootstrap-table
    $('tr.clickligne').click(function () {
        window.location=$(this).data("href");
    });
    // eof bootstrap-table

    // bof Retour en haut de page
    // Affichage de la flêche
    $(window).scroll(function() {
        if($(window).scrollTop() == 0){
            $('#scrollToTop').fadeOut("fast");
        } else {
            $('#scrollToTop').fadeIn("slow");
        }
    });
    // Action de retour en haut de page
    $('#scrollToTop a').click(function(event){
        event.preventDefault();
        $('html, body').animate({scrollTop:0},800 , 'swing');
        return false;
    });
    // eof Affichage de la flêche Retour en haut de page

    $('.attendre-archive').one("click", function () {
        $(this).parents('h1').append('<div class="float-right attendre">Recherche des archives en cours. Merci de patienter... <i class="far fa-spinner fa-spin"></i></div>');
        $('.page-header h1').css('overflow', 'hidden');
        $('.attendre').animate({
            width: '500px'
        }, 2000, "linear")
    });

});