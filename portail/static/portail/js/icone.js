function AfficherIcones() {

    /*  Ce fichier centralise l'affichage des toutes les icônes (balise <i>).
        Son fonctionnement repose sur l'utilisation du nom d'une classe personnalisée : <i class="user"></i>
        Le code ci-dessous parcours toutes les balises <i> de la page, si la 1ère classe définie est identifiée,
        alors la balise est remplacée par sa nouvelle définition.
        NB : les classes de la balise d'origine sont automatiquement réaffectées à la nouvelle.
    */

    $("i").each(function() {

        new_i = null;
        switch ($(this)[0].classList[0]) {

            // Pictos généraux

            case "utilisateur":
                new_i = '<i class="fas fa-user fa-fw"></i>';
                break;

            case "organisateur":
                new_i = '<i class="fas fa-user-ninja fa-fw" title="Organisateur" data-toggle="tooltip"></i>';
                break;

            case "instructeur":
                new_i = '<i class="fas fa-user-secret fa-fw" title="Instructeur" data-toggle="tooltip"></i>';
                break;

            case "structure":
                new_i = '<i class="fas fa-building fa-fw" title="Structure" data-toggle="tooltip"></i>';
                break;

            case "password":
                new_i = '<i class="fas fa-lock fa-fw"></i>';
                break;

            case "tableau-de-bord":
                new_i = '<i class="fas fa-tachometer-alt fa-fw" title="Tableau de bord" data-toggle="tooltip"></i>';
                break;

            case "aide":
                new_i = '<i class="fas fa-question-square fa-fw"></i>';
                break;

            case "aide-contextuelle":
                new_i = '<i class="fas fa-info-circle" style="color: #9AA400; fa-fw"></i>';
                break;

            case "nouveaute":
                new_i = '<i class="far fa-bullhorn fa-fw"></i>';
                break;

            case "historique":
                new_i = '<i class="fas fa-history fa-fw"></i>';
                break;

            case "suivi":
                new_i = '<i class="fas fa-random fa-fw"></i>';
                break;

            case "lien":
                new_i = '<i class="fas fa-link fa-fw"></i>';
                break;

            case "spinner":
                new_i = '<i class="far fa-spinner fa-spin"></i>';
                break;

            // Pictos liés aux éléments des dossiers

            case "dossier":
                new_i = '<i class="fas fa-folder fa-fw"></i>';
                break;

            case "dossier-annexe":
                new_i = '<i class="fas fa-layer-group fa-fw"></i>';
                break;

            case "detail":
                new_i = '<i class="far fa-tasks fa-fw"></i>';
                break;

            case "doc":
                new_i = '<i class="far fa-paperclip fa-fw"></i>';
                break;

            case "archive":
                new_i = '<i class="far fa-file-archive fa-fw" title="Archive" data-toggle="tooltip"></i>';
                break;

            case "message":
                new_i = '<i class="far fa-envelope fa-fw"></i>';
                break;

            case "date":
                new_i = '<i class="far fa-calendar-alt fa-fw"></i>';
                break;

            case "ville":
                new_i = '<i class="far fa-map-marker-alt fa-fw"></i>';
                break;

            case "activite":
                new_i = '<i class="far fa-pennant fa-fw" title="Activité" data-toggle="tooltip"></i>';
                break;

            case "telephone":
                new_i = '<i class="far fa-phone fa-fw"></i>';
                break;

            case "euro":
                new_i = '<i class="far fa-euro-sign fa-fw"></i>';
                break;

            case "lieu":
                new_i = '<i class="far fa-flag fa-fw"></i>';
                break;

            case "diplome":
                new_i = '<i class="far fa-wreath fa-fw"></i>';
                break;

            case "feuille":
                new_i = '<i class="fab fa-pagelines fa-fw"></i>';
                break;

            case "charte_grp0":
                new_i = '<i class="far fa-flower-tulip fa-lg fa-fw"></i>';
                break;

            case "charte_grp1":
                new_i = '<i class="far fa-cheeseburger fa-lg fa-fw"></i>';
                break;

            case "charte_grp2":
                new_i = '<i class="fas fa-exchange fa-lg fa-fw"></i>';
                break;

            case "charte_grp3":
                new_i = '<i class="fas fa-bus fa-lg fa-fw"></i>';
                break;

            case "charte_grp4":
                new_i = '<i class="far fa-handshake fa-lg fa-fw"></i>';
                break;

            // Pictos liés aux actions de l'utilisateur

            case "sincrire":
                new_i = '<i class="fas fa-user-plus"></i>';
                break;

            case "connecter":
                new_i = '<i class="fas fa-sign-in-alt fa-fw"></i>';
                break;

            case "deconnecter":
                new_i = '<i class="fas fa-sign-out-alt fa-fw"></i>';
                break;

            case "retour":
                new_i = '<i class="far fa-reply fa-fw"></i>';
                break;

            case "action":
                new_i = '<i class="far fa-magic fa-fw"></i>';
                break;

            case "action-requise":
                new_i = '<i class="fas fa-exclamation-triangle text-danger fa-fw"></i>';
                break;

            case "action-requise-blanc":
                new_i = '<i class="fas fa-exclamation-triangle text-light fa-fw"></i>';
                break;

            case "ajouter":
                new_i = '<i class="far fa-plus-hexagon fa-fw"></i>';
                break;

            case "a-completer":
                new_i = '<i class="fal fa-long-arrow-left fa-fw"></i><i class="fas fa-exclamation-triangle text-danger fa-fw"></i>à completer';
                break;

            case "evaluer":
                new_i = '<i class="far fa-chart-line fa-fw"></i>';
                break;

            case "imprimer":
                new_i = '<i class="far fa-print fa-fw"></i>';
                break;

            case "telecharger":
                new_i = '<i class="far fa-download fa-fw"></i>';
                break;

            case "editer":
                title = ($(this).data('titre')) ? $(this).data('titre') : "";
                select = ($(this).data('selection')) ? $(this).data('selection') : "";
                new_i = '<i class="far fa-edit fa-fw" title="' + title + '" data-toggle="tooltip"></i> \
                    <span hidden>' + select + '</span>';
                break;

            case "envoyer":
                title = ($(this).data('titre')) ? $(this).data('titre') : "";
                select = ($(this).data('selection')) ? $(this).data('selection') : "";
                new_i = '<i class="far fa-paper-plane fa-fw" title="' + title + '" data-toggle="tooltip"></i> \
                    <span hidden>' + select + '</span>';
                break;

            case "notifier":
                title = ($(this).data('titre')) ? $(this).data('titre') : "";
                select = ($(this).data('selection')) ? $(this).data('selection') : "";
                new_i = '<i class="far fa-broadcast-tower fa-fw" title="' + title + '" data-toggle="tooltip"></i> \
                    <span hidden>' + select + '</span>';
                break;

            case "supprimer":
                new_i = '<i class="far fa-trash-alt fa-fw"></i>';
                break;

            case "configurer":
                new_i = '<i class="far fa-wrench fa-fw"></i>';
                break;

            case "deplier":
                new_i = '<i class="fal fa-eye fa-fw"></i>';
                break;

            case "haut-de-page":
                new_i = '<i class="fal fa-angle-up fa-fw" title="Remonter au haut de page" data-toggle="tooltip"></i>';
                break;

            // Pictos liés aux états des dossier

            case "diplome_or":
                new_i = '<span class="fa-stack fa-lg" title="Charte diplômée OR" data-toggle="tooltip"> \
                    <i class="fas fa-certificate fa-stack-2x"></i> \
                    <i class="fas fa-trophy fa-stack-1x" style="color: goldenrod"></i> \
                </span>';
                break;
            case "diplome_arg":
                new_i = '<span class="fa-stack fa-lg" title="Charte diplômée ARGENT" data-toggle="tooltip"> \
                    <i class="fas fa-certificate fa-stack-2x"></i> \
                    <i class="fas fa-trophy fa-stack-1x" style="color: #BFBFBF"></i> \
                </span>';
                break;
            case "diplome_bro":
                new_i = '<span class="fa-stack fa-lg" title="Charte diplômée BRONZE" data-toggle="tooltip"> \
                    <i class="fas fa-certificate fa-stack-2x"></i> \
                    <i class="fas fa-trophy fa-stack-1x" style="color: #C07949"></i> \
                </span>';
                break;
            case "diplome_echec":
                new_i = '<span class="fa-stack fa-lg" title="Charte NON diplômée" data-toggle="tooltip"> \
                    <i class="fas fa-frown fa-stack-2x"></i> \
                </span>';
                break;

            // Pictos liés aux états des éléments

            case "attente":
                new_i = '<i class="far fa-hourglass text-primary fa-fw"></i>';
                break;

            case "ok":
                new_i = '<i class="far fa-check text-success fa-fw"></i>';
                break;

            case "ok-blanc":
                new_i = '<i class="far fa-check fa-fw"></i>';
                break;

            case "pas-ok":
                new_i = '<i class="far fa-ban text-danger fa-fw"></i>';
                break;

            case "pas-ok-blanc":
                new_i = '<i class="far fa-ban fa-fw"></i>';
                break;

            case "commentaire":
                new_i = '<i class="far fa-comment-dots text-warning fa-fw"></i>';
                break;

            default:
                console.log("Classe personnalisée d'icône non trouvée : " + $(this)[0].classList[0]);
        }

        if (new_i != null) {
            $(this).replaceWith($(new_i).addClass($(this)[0].className)); // affecte les classes CSS définie à l'origine sur nouvel élément
            $('[data-toggle="tooltip"]').tooltip({'html': true});
        }

    });
}



$(document).ready(function () {
    AfficherIcones();
});