# coding: utf-8
from django.apps import AppConfig


class OrganisateursConfig(AppConfig):
    """ Configuration d'Application """

    # Configuration
    name = 'organisateurs'
    verbose_name = "Organisateurs et structures"

    # Actions
    def ready(self):
        pass


default_app_config = 'organisateurs.OrganisateursConfig'
