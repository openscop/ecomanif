# coding: utf-8
from django.shortcuts import render, get_object_or_404

from core.util.permissions import require_role
from evenements.models.manifestation import Manif


def verifier_proprietaire(function=None):
    """ Décorateur : limite l'accès aux organisateurs d'un événement """

    def decorator(view_func):
        @require_role('organisateur')
        def _wrapped_view(request, *args, **kwargs):
            if 'pk' in kwargs:
                pk = kwargs['pk']
            elif 'manif_pk' in kwargs:
                pk = kwargs['manif_pk']
            else:
                pk = None
            manif = get_object_or_404(Manif, pk=pk)
            if request.user == manif.structure.organisateur.user:  # noqa
                return view_func(request, *args, **kwargs)
            else:
                return render(request, "core/notification.html",
                              {'title': "Accès restreint", 'type': 1,
                               'message': "Vous n'avez pas les droits d'accès à cette page"})

        return _wrapped_view

    if function:
        return decorator(function)
    return decorator
