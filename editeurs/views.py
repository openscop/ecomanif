from django.utils.decorators import method_decorator
from django.views.generic import ListView, UpdateView, CreateView, DeleteView
from django.shortcuts import render, get_object_or_404
from extra_views import CreateWithInlinesView, UpdateWithInlinesView
from django.contrib.flatpages.models import FlatPage
from django.http import HttpResponseRedirect

from .forms import PageForm, PageAideForm
from core.util.permissions import require_role
from aide.models import HelpPage
from aide.forms.help import HelpNoteInLine
from django.contrib.sites.models import Site


class PageListView(ListView):
    """ Liste des pages statiques """
    model = FlatPage
    template_name = 'editeurs/pageliste.html'

    @method_decorator(require_role('editeur'))
    def dispatch(self, request, *args, **kwargs):
        if request.user.default_instance.departement:
            return render(request, "core/notification.html",
                          {'title': "Accès restreint", 'type': 1,
                           'message': "Vous n'avez pas les droits d'accès à cette page"})
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['stat'] = True
        return context


class PageEditionView(UpdateView):
    """ Edition des pages statiques """
    model = FlatPage
    form_class = PageForm
    template_name = 'editeurs/pageform.html'

    @method_decorator(require_role('editeur'))
    def dispatch(self, request, *args, **kwargs):
        if request.user.default_instance.departement:
            return render(request, "core/notification.html",
                          {'title': "Accès restreint", 'type': 1,
                           'message': "Vous n'avez pas les droits d'accès à cette page"})
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['stat'] = True
        return context


@method_decorator(require_role('editeur'), name='dispatch')
class PageCreationView(CreateView):
    """ Création des pages statiques """
    model = FlatPage
    form_class = PageForm
    template_name = 'editeurs/pageform.html'

    @method_decorator(require_role('editeur'))
    def dispatch(self, request, *args, **kwargs):
        if request.user.default_instance.departement:
            return render(request, "core/notification.html",
                          {'title': "Accès restreint", 'type': 1,
                           'message': "Vous n'avez pas les droits d'accès à cette page"})
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['stat'] = True
        return context

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.save()
        self.object.sites.set(Site.objects.all().values_list('pk', flat=True))
        form.save_m2m()
        return HttpResponseRedirect(self.get_success_url())


class PageSuppressionView(DeleteView):
    """ Création des pages statiques """
    model = FlatPage
    template_name = 'editeurs/pagedelete.html'
    success_url = '/edition/liste_stat'

    @method_decorator(require_role('editeur'))
    def dispatch(self, request, *args, **kwargs):
        if request.user.default_instance.departement:
            return render(request, "core/notification.html",
                          {'title': "Accès restreint", 'type': 1,
                           'message': "Vous n'avez pas les droits d'accès à cette page"})
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['stat'] = True
        return context


@method_decorator(require_role('editeur'), name='dispatch')
class PageAideListView(ListView):
    """ Liste des pages statiques """
    model = HelpPage
    template_name = 'editeurs/pageliste.html'

    def get_queryset(self):
        if self.request.user.default_instance.departement:
            queryset = HelpPage.objects.filter(departements=self.request.user.default_instance.departement).order_by('path')
        else:
            queryset = HelpPage.objects.all().order_by('path')
        return queryset


@method_decorator(require_role('editeur'), name='dispatch')
class PageAideEditionView(UpdateWithInlinesView):
    """ Edition des pages statiques """
    model = HelpPage
    form_class = PageAideForm
    inlines = [HelpNoteInLine]
    template_name = 'editeurs/pageform.html'


@method_decorator(require_role('editeur'), name='dispatch')
class PageAideCreationView(CreateWithInlinesView):
    """ Création des pages statiques """
    model = HelpPage
    form_class = PageAideForm
    inlines = [HelpNoteInLine]
    template_name = 'editeurs/pageform.html'

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        if self.request.user.default_instance.departement:
            kwargs['initial'].update({'departements': self.request.user.default_instance.departement})
        return kwargs


@method_decorator(require_role('editeur'), name='dispatch')
class PageAideSuppressionView(DeleteView):
    """ Création des pages statiques """
    model = HelpPage
    template_name = 'editeurs/pagedelete.html'
    success_url = '/edition/liste_perso'
