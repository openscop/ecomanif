# coding: utf-8
from django.urls import path, re_path

from .views import PageListView, PageEditionView, PageCreationView, PageSuppressionView
from .views import PageAideListView, PageAideEditionView, PageAideCreationView, PageAideSuppressionView


app_name = 'editeurs'
urlpatterns = [

    path('liste_stat', PageListView.as_view(), name='liste_pagestat'),
    path('creation_stat', PageCreationView.as_view(), name='creation_pagestat'),
    re_path('edit_stat/(?P<pk>\d+)', PageEditionView.as_view(), name='edition_pagestat'),
    re_path('delete_stat/(?P<pk>\d+)', PageSuppressionView.as_view(), name='suppression_pagestat'),
    path('liste_perso', PageAideListView.as_view(), name='liste_pageperso'),
    path('creation_perso', PageAideCreationView.as_view(), name='creation_pageperso'),
    re_path('edit_perso/(?P<pk>\d+)', PageAideEditionView.as_view(), name='edition_pageperso'),
    re_path('delete_perso/(?P<pk>\d+)', PageAideSuppressionView.as_view(), name='suppression_pageperso'),
]