from django.contrib.flatpages.models import FlatPage
from django.forms.fields import TypedMultipleChoiceField
from django import forms
from ckeditor_uploader.widgets import CKEditorUploadingWidget

from aide.models import HelpPage
from core.util.user import UserHelper


class PageForm(forms.ModelForm):
    """ Formulaire d'édition / création des pages statiques """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.base_fields['registration_required'].label='Accès authentifié uniquement'

    class Meta:
        model = FlatPage
        exclude = ['pk', 'enable_comments', 'template_name', 'sites']
        widgets = {'content': CKEditorUploadingWidget()}


class PageAideForm(forms.ModelForm):
    """ Formulaire d'édition / création des pages d'aide """

    # Champs
    role = TypedMultipleChoiceField(choices=UserHelper.ROLE_CHOICES, required=False, label="Rôle")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if 'role' in self.initial and self.initial['role']:
            self.initial['role'] = self.initial['role'].split(',')

    def save(self, commit=True):
        """Sauvegarde de l'objet"""
        self.instance.role = ','.join(sorted(self.cleaned_data['role']))
        return super().save(commit=commit)

    class Meta:
        model = HelpPage
        exclude = ['pk']
        widgets = {'content': CKEditorUploadingWidget()}
