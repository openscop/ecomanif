# coding: utf-8
from django.urls import re_path

from .views import PNList, ADMList


app_name = 'parcs_naturels'
urlpatterns = [

    # Listes des RNR et sites Natura2000
    re_path('admin', ADMList.as_view(), name='adm_list'),
    re_path('', PNList.as_view(), name='pn_list'),
]
