# coding: utf-8
from django.apps import AppConfig


class ParcsNaturelsConfig(AppConfig):
    """ Configuration de l'application """

    name = 'parcs_naturels'
    verbose_name = "Parcs Naturels"

    def ready(self):
        """ Installer les récepteurs de signaux (listeners) """
        pass


default_app_config = 'parcs_naturels.ParcsNaturelsConfig'
