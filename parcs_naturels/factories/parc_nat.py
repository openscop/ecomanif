# coding: utf-8
import factory

from administrative_division.factories import DepartementFactory
from ..models import ParcNaturel, AdministrateurParcNaturel


class PNFactory(factory.django.DjangoModelFactory):
    """ Factory parc naturel régional ou national """

    # Champs
    name = "Gorges de la Loire"
    departement = factory.SubFactory(DepartementFactory)

    # Méta
    class Meta:
        model = ParcNaturel


class AdministrateurRNRFactory(factory.django.DjangoModelFactory):
    """ Factory administrateur de RNR """

    # Champs
    short_name = 'FRAPNA'
    name = "Federation Rhone-Alpes de Protection de la Nature de la Loire"
    email = 'john.doe@frapna.org'
    parcnat = factory.SubFactory(PNFactory)
    departement = factory.SubFactory(DepartementFactory)

    # Meta
    class Meta:
        model = AdministrateurParcNaturel
