# coding: utf-8
from django.contrib import admin
from django import forms
from django.urls import reverse_lazy
from django.forms.models import ModelChoiceField
from import_export.admin import ImportExportActionModelAdmin
from clever_selects.form_fields import ChainedModelChoiceField
from related_admin import RelatedFieldAdmin

from contacts.admin import ContactInline
from core.util.admin import RelationOnlyFieldListFilter
from ..models import ParcNaturel, AdministrateurParcNaturel
from administrative_division.models import Departement, Commune


class AdministrateurParcNaturelAdminForm(forms.ModelForm):
    """ Formulaire d'admin des administrateurs de parcs """

    # Champs
    departement = ModelChoiceField(required=False, queryset=Departement.objects.all(), label="Département")
    commune = ChainedModelChoiceField('departement', reverse_lazy('administrative_division:commune_widget'),
                                      Commune, label="Commune")

    # Overrides
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.instance.pk and self.instance.commune:
            self.fields['departement'].initial = self.instance.commune.get_departement()
            self.fields['commune'].queryset = Commune.objects.by_departement(self.instance.commune.get_departement())

    # Meta
    class Meta:
        model = AdministrateurParcNaturel
        exclude = []


@admin.register(AdministrateurParcNaturel)
class AdministrateurParcNaturelAdmin(ImportExportActionModelAdmin, RelatedFieldAdmin):
    """ Configuration admin """

    list_display = ['pk', 'name', 'parcnat']
    list_filter = ['parcnat', ('departement', RelationOnlyFieldListFilter)]
    search_fields = ['name', 'parcnat__name']
    inlines = [ContactInline]
    form = AdministrateurParcNaturelAdminForm
    list_per_page = 25

    # Overrides
    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj=None, **kwargs)
        if request.user.get_departement():
            form.base_fields['parcnat'].queryset = ParcNaturel.objects.filter(departements=request.user.get_departement())
        return form


@admin.register(ParcNaturel)
class ParcNaturelAdmin(ImportExportActionModelAdmin, RelatedFieldAdmin):
    """ Configuration RNR """

    list_display = ['code', 'name', 'departements_concernes']
    list_filter = [('departements', RelationOnlyFieldListFilter)]
    search_fields = ['name']
    list_per_page = 25

    # Overrides
    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj=None, **kwargs)
        form.base_fields['departements'].queryset = Departement.objects.configured()
        return form

    def departements_concernes(self, obj):
        deps = obj.departements.all()
        list_dep = ''
        for dep in deps:
            if list_dep == '':
                list_dep += dep.name
            else:
                list_dep += ' - ' + dep.name
        return list_dep
