# coding: utf-8
from django.views.generic import ListView

from .models import ParcNaturel
from administrative_division.models.departement import Departement


class PNList(ListView):
    """ Afficher la liste des réserves naturelles régionales """

    # Configuration
    model = ParcNaturel

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        departements = Departement.objects.configured()
        context['departements'] = departements.order_by('name')
        return context


class ADMList(ListView):
    """ Afficher la liste des administrateurs de parcs """

    model = ParcNaturel
    template_name = 'parcs_naturels/administrateurparcnaturel_list.html'
