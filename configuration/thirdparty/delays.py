# Delays concerning validation workflows
import datetime

FEDERAL_AGREEMENT_DELAY = datetime.timedelta(weeks=4)
AGREEMENT_DELAY = datetime.timedelta(weeks=3)
