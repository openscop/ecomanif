# coding: utf-8
from django.urls import path, re_path

from core.views.user import signuporganisateurview


app_name = 'core'
urlpatterns = [

    path('organisateur', signuporganisateurview, name='inscription_organisateur'),
]
