# coding: utf-8

from os import path

from django import template


register = template.Library()


@register.filter
def basename(value):
    """ Renvoyer le nom de fichier seul depuis un chemin """
    if isinstance(value, str):
        return path.basename(value)
    else:
        return basename(str(value))
