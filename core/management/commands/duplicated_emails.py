# coding: utf-8
from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    def handle(self, *args, **options):
        duplicated_emails = 0
        for user in get_user_model().objects.all():
            current_email = get_user_model().objects.filter(email=user.email)
            if current_email.count() > 1:
                duplicated_emails += 1
                print("\n==={} :".format(current_email[0].email))
                print(" - ".join([duplicated.username for duplicated in current_email]))
                print(" - ".join([str(mail) for mail in user.emailaddress_set.all()]))
                print("===")
        print("Duplicated emails : {0:n}".format(duplicated_emails / 2))
