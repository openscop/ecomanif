# coding: utf-8
from django.contrib.auth import hashers
from django.test import TestCase

from core.factories.user import UserFactory


class TestUsers(TestCase):

    # Configuration des tests individuels
    def setUp(self):
        pass

    # Tests
    def test_drupal_hashing(self):
        hashers.get_hashers()
        encoded = hashers.make_password('formationddcs', 'wX89/crt', 'drupal_sha512')
        self.assertEqual(encoded, 'drupal_sha512$32768$wX89/crt$w46C97GO3Qd/Zkx/qE5BBrtVuj687bmeTIC9b56tpGC')
        self.assertTrue(hashers.is_password_usable(encoded))
        self.assertTrue(hashers.check_password('formationddcs', encoded))
        self.assertFalse(hashers.check_password('formationddcse', encoded))
        self.assertEqual(hashers.identify_hasher(encoded).algorithm, "drupal_sha512")

        # Blank passwords
        blank_encoded = hashers.make_password('', 'seasalt', 'drupal_sha512')
        self.assertTrue(blank_encoded.startswith('drupal_sha512$'))
        self.assertTrue(hashers.is_password_usable(blank_encoded))
        self.assertTrue(hashers.check_password('', blank_encoded))
        self.assertFalse(hashers.check_password(' ', blank_encoded))

    def test_user_instance(self):
        """ Tester la récupération d'instance """
        user = UserFactory.create()
        instance = user.get_instance()
        self.assertIsNotNone(instance)
