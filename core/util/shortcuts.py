# coding: utf-8
import importlib


def import_fullname(name):
    """
    Importer un attribut d'un module

    :param name: Nom pleinement qualifié du module, ex. os.path.join
    :returns: module ou attribut d'un module
    """
    name, member = name.rsplit(".", 1)
    mod = importlib.import_module(name)
    return getattr(mod, member)
