# coding: utf-8
from django.shortcuts import redirect
from django.contrib import messages
from django.urls import reverse
from django import forms
import re

from allauth.account.models import EmailAddress
from allauth.exceptions import ImmediateHttpResponse
from allauth.account.adapter import DefaultAccountAdapter
from allauth.socialaccount.adapter import DefaultSocialAccountAdapter
from allauth.socialaccount.providers.base import AuthProcess

from core.util.user import UserHelper


class EcomanifAccountAdapter(DefaultAccountAdapter):
    """ Account adapter (Django Allauth) """

    def get_login_redirect_url(self, request):
        """ Renvoyer l'URL de redirection après connexion """

        # Utiliser le mécanisme par défaut pour la validation mais ne pas renvoyer l'URL
        base_home = super().get_login_redirect_url(request)
        user = request.user
        if UserHelper.has_role(user, 'organisateur'):
            return reverse('evenements:tableau-de-bord-organisateur')
        if UserHelper.has_role(user, 'referent'):
            return reverse('referents:tableau-de-bord-referent')
        elif UserHelper.has_role(user, 'editeur'):
            return reverse('editeurs:liste_pagestat')
        else:
            if 'callback' in request.path:
                return reverse('core:inscription_organisateur')
            return base_home

    def clean_username(self, username, shallow=False):
        """ Renvoyer un nom d'utilisateur valide depuis un nom d'utilisateur passé """
        if not re.match(r'^[a-zA-Z0-9]+$', username):
            raise forms.ValidationError("Le nom d'utilisateur ne peut contenir que des lettres non-accentuées et des chiffres")
        if len(username) < 3:  # Minimum 3 caractères (correspond aux limites de l'API Openrunner)
            raise forms.ValidationError("Le nom d'utilisateur doit contenir au moins 3 (trois) caractères")
        return super().clean_username(username, shallow)


class EcomanifSocialAccountAdapter(DefaultSocialAccountAdapter):

    def pre_social_login(self, request, sociallogin):
        request.session['provider'] = sociallogin.account.provider
        if 'socialaccount_nonce' in request.session:
            request.session.pop('socialaccount_nonce')

        # Gestion du cas nouvelle connexion avec un compte social qui a une email déjà utilisée
        # si déjà enregistré, on laisse passer
        if sociallogin.is_existing:
            return
        # si pas de email dans les données reçues, gérer plus loin
        if 'email' not in sociallogin.account.extra_data:
            return
        # si on est sur un connect, on veut relier un compte, on laisse passer
        process = sociallogin.state.get('process')
        if process == AuthProcess.REDIRECT or process == AuthProcess.CONNECT:
            return
        # recherche si email existe, alors redirect sur écran login avec message d'erreur
        try:
            email = sociallogin.account.extra_data['email'].lower()
            email_address = EmailAddress.objects.get(email__iexact=email)

        except EmailAddress.DoesNotExist:
            return

        messages.error(request, "Un compte existe déjà avec l'adresse de couriel : "+email_address.email+
                       "... Connectez vous avec votre compte local et relier votre compte social dans la page profile.")
        raise ImmediateHttpResponse(redirect('/accounts/login/'))
