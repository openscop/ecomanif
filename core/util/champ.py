from django import forms


class TelField(forms.CharField):
    """
    Classe spéciale pour les numéros de téléphone
        Formats acceptés :
            0x xx xx xx xx
            0x.xx.xx.xx.xx
            0xxxxxxxxx
        Format sauvegardé : 0x xx xx xx xx
    """

    def clean(self, value):
        if len(value) < 10 or len(value) > 14:
            raise forms.ValidationError('La longueur doit être comprise entre 10 et 14 caractères')
        if value[0] != '0':
            raise forms.ValidationError('Le numéro doit commencer par 0')
        value = "".join(value.split())  # Supprimer les espaces
        value = "".join(value.split('.'))  # Supprimer les points
        try:
            int(value)
        except ValueError:
            raise forms.ValidationError("ce champ ne doit comporter que des chiffres !")
        return '%s %s %s %s %s' % (
            value[0:2],
            value[2:4],
            value[4:6],
            value[6:8],
            value[8:10]
        )
