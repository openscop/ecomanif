# coding: utf-8
from django.contrib import admin

from core.models.instance import Instance


@admin.register(Instance)
class InstanceAdmin(admin.ModelAdmin):
    """ Administration des instances de configuration """

    # Configuration
    list_display = ['pk', 'name', 'departement', 'manifestation_delay', 'global_argent', 'global_or']
    list_filter = ['instruction_mode']
    search_fields = ['name']
    list_per_page = 25
    fields = (('name', 'departement'), 'manifestation_delay',
              ('group1_argent', 'group2_argent', 'group3_argent', 'group4_argent', 'group5_argent'),
              ('group1_or', 'group2_or', 'group3_or', 'group4_or', 'group5_or'),
              ('global_argent', 'global_or'), ('sender_email', 'email_settings'))
