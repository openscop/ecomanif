# coding: utf-8
from django.contrib.admin.filters import SimpleListFilter

from core.util.user import UserHelper


class RoleFilter(SimpleListFilter):
    """ Filtre admin par rôle d'utilisateur """

    title = "Rôle"
    parameter_name = 'roles'

    def lookups(self, request, model_admin):
        """ Renvoyer les valeurs possibles du filtre """
        lookups = UserHelper.ROLE_CHOICES
        return lookups

    def queryset(self, request, queryset):
        """ Renvoyer un queryset des utilisateurs correspondant à un rôle """
        if self.value() in UserHelper.DIRECT_TYPES:
            lookup = {'{0}__isnull'.format(self.value()): False}
            return queryset.filter(**lookup)
        return queryset
