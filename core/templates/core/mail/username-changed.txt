Bonjour,

Nous vous informons que suite à la mise à jour vers la plateforme nationale,
votre nom d'utilisateur a dû subir une modification. Vous pourrez désormais
vous connecter à la plateforme en utilisant le nom d'utilisateur suivant :

        {{ username }}

=== VEUILLEZ NE PAS RÉPONDRE À CET E-MAIL ===
Pour effectuer la suite de votre démarche :
- Connectez vous sur la plateforme Manifestation Sportive ;
- rendez-vous sur votre tableau de bord.
Rendez-vous à l'adresse {{url}}
