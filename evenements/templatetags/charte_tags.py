from django import template

register = template.Library()


@register.filter
def iconic(value):
    """ Remplacer la valeur True/False par une icône """
    if value:
        return '<i class="fas fa-smile-beam fa-lg text-success"></i>'
    return '<i class="fas fa-times fa-lg text-danger"></i>'
