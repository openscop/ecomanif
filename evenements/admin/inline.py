# coding: utf-8
from django.contrib import admin

from ..models import DocumentComplementaire, Charte


class DocumentComplementaireInline(admin.TabularInline):
    """ Inline des documents complémentaires """

    # C,onfiguration
    model = DocumentComplementaire
    extra = 0


class CharteInLine(admin.StackedInline):
    """ Inline de la charte associée """

    model = Charte
    extra = 0
    fieldsets = (
        (Charte.CHARTE_GROUP[0], {'classes': ('collapse',),'fields': Charte.CHARTE_GROUP_MEMBER[0]}),
        (Charte.CHARTE_GROUP[1], {'classes': ('collapse',), 'fields': Charte.CHARTE_GROUP_MEMBER[1]}),
        (Charte.CHARTE_GROUP[2], {'classes': ('collapse',), 'fields': Charte.CHARTE_GROUP_MEMBER[2]}),
        (Charte.CHARTE_GROUP[3], {'classes': ('collapse',), 'fields': Charte.CHARTE_GROUP_MEMBER[3]}),
        (Charte.CHARTE_GROUP[4], {'classes': ('collapse',), 'fields': Charte.CHARTE_GROUP_MEMBER[4]}),
    )
    template = "admin/charte.html"
