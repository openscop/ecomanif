# coding: utf-8
from django.contrib import admin
from django.db.models import Q
from django.utils import timezone


class CalendrierFilter(admin.SimpleListFilter):
    title = 'Présence dans le calendrier'
    parameter_name = 'Dans_calendrier'

    def lookups(self, request, model_admin):
        return ('True', 'Oui'), ('False', 'Non')

    def queryset(self, request, queryset):
        if self.value() == 'True':
            return queryset.filter(Q(prive=False) & Q(cache=False))
        if self.value() == 'False':
            return queryset.filter(Q(prive=True) | Q(cache=True))


class AVenirFilter(admin.SimpleListFilter):
    title = 'A venir'
    parameter_name = 'a_venir'

    def lookups(self, request, model_admin):
        return ('True', 'Oui'), ('False', 'Passée')

    def queryset(self, request, queryset):
        if self.value() == 'True':
            return queryset.filter(date_debut__gte=timezone.now())
        if self.value() == 'False':
            return queryset.filter(date_debut__lt=timezone.now())
