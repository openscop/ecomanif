# coding: utf-8
import logging
from datetime import timedelta

from ajax_select.fields import AutoCompleteField
from bootstrap_datepicker_plus import DateTimePickerInput as BaseDateTimePickerInput
from django import forms
from django.core.exceptions import ValidationError
from django.urls import reverse_lazy
from django.forms import ModelChoiceField
from django.forms.models import ModelMultipleChoiceField
from django.utils import timezone
from crispy_forms.layout import Layout, Fieldset, HTML

from administrative_division.models.commune import Commune
from administrative_division.models.departement import Departement
from clever_selects.form_fields import ChainedModelChoiceField, ChainedModelMultipleChoiceField
from clever_selects.forms import ChainedChoicesModelForm
from core.forms.base import GenericForm
from ..models.manifestation import Manif
from sports.models import Discipline
from sports.models.sport import Activite


logger = logging.getLogger('django.request')

# Contenus de Fieldsets standards
# Modifiés à la volée dans les helpers des formulaires hérités
FIELDS_MANIFESTATION = ["Détail de la manifestation", 'nom', 'date_debut', 'date_fin', 'description', 'extra_activite',
                        'discipline', 'activite', 'departement_depart', 'ville_depart', 'nb_participants',
                        'nb_spectateurs', 'parcs', 'departements_traverses', 'villes_traversees',
                        'public', 'afficher_adresse_structure']
FIELDS_FILES = ["Fichiers joints", 'cartographie', 'convention', 'docs_additionels']
FIELDS_FILES_optionals = []
FIELDS_CONTACT = ["Personne à contacter sur place", 'nom_contact', 'prenom_contact', 'tel_contact', 'email_contact']

# Dates par défaut de début et fin de manifestation
DEFAULT_START = timezone.now() + timedelta(days=90)
# Inversion jour et mois pour datepicker4
DEFAULT_START_STR = DEFAULT_START.strftime('%m/%d/%Y 8:00')
DEFAULT_END_STR = DEFAULT_START.strftime('%m/%d/%Y 20:00')


class DateTimePickerInput(BaseDateTimePickerInput):
    """
    Surcharge du widget pour inclure jquery (erreur dans console JS après upgrade général)
    """
    class Media:
        """JS/CSS resources needed to render the date-picker calendar."""

        js = (
            '/static/libs/jquery.min.js',
            'https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/'
            'moment-with-locales.min.js',
            'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/'
            '4.17.47/js/bootstrap-datetimepicker.min.js',
            'bootstrap_datepicker_plus/js/datepicker-widget.js'
        )
        css = {'all': (
            'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/'
            '4.17.47/css/bootstrap-datetimepicker.css',
            'bootstrap_datepicker_plus/css/datepicker-widget.css'
        ), }


# Widgets
FORM_WIDGETS = {
    'date_debut': DateTimePickerInput(options={"format": "DD/MM/YYYY HH:mm", 'defaultDate': DEFAULT_START_STR, 'locale': 'fr'}),
    'date_fin': DateTimePickerInput(options={"format": "DD/MM/YYYY HH:mm", 'defaultDate': DEFAULT_END_STR, 'locale': 'fr'}),
    'description': forms.Textarea(attrs={'rows': 1}),
}

# Textes d'aide
HELP_EXTRA_ACTIVITE = "recherchez votre activité sportive dans ce champ. " \
                      "Cliquez ensuite sur l'activité pour remplir automatiquement les champs ci-dessous."


class ManifForm(GenericForm, ChainedChoicesModelForm):
    """ Création de manifestation """

    # Champs
    extra_activite = AutoCompleteField('activite', label="Recherche d'activité sportive", required=False,
                                       help_text=HELP_EXTRA_ACTIVITE, show_help_text=False)
    discipline = ModelChoiceField(required=False, queryset=Discipline.objects.all())
    activite = ChainedModelChoiceField('discipline', reverse_lazy('sports:activite_widget'), Activite, required=False)
    departement_depart = ModelChoiceField(required=False, queryset=Departement.objects.configured(), label="Département de départ")
    ville_depart = ChainedModelChoiceField('departement_depart', reverse_lazy('administrative_division:commune_widget'), Commune,
                                           label="Commune de départ", required=False)
    departements_traverses = ModelMultipleChoiceField(required=False, queryset=Departement.objects.all(),
                                                      label="Autres départements traversés")
    villes_traversees = ChainedModelMultipleChoiceField('departements_traverses',
                                                        reverse_lazy('administrative_division:commune_widget'),
                                                        Commune, label="Autres communes traversées", required=False,
                                                        help_text="Ne pas sélectionner la commune de départ.")
    public = forms.BooleanField(initial=True, label="Publier la manifestation dans le calendrier", required=False)

    # Overrides
    def __init__(self, *args, **kwargs):
        self.extradata = kwargs.pop('extradata', None)
        if hasattr(kwargs['instance'], 'prive') and kwargs['instance'].prive:
            kwargs['initial']['public'] = False
        super().__init__(*args, **kwargs)
        self.helper.layout = Layout(HTML("<p>Remplissez ce formulaire puis vous pourrez joindre "
                                         "ensuite des fichiers à votre dossier.</p><hr>"),
                                    Fieldset(*FIELDS_MANIFESTATION),
                                    Fieldset(*FIELDS_CONTACT),
                                    )
        if hasattr(self, 'helper'):
            self.helper.form_tag = False
        self.fields['ville_depart'].widget.attrs = {'data-placeholder': "Sélectionnez d'abord un département"}
        self.fields['departements_traverses'].widget.attrs = {'data-placeholder': "1. Sélectionnez les départements traversés"}
        self.fields['villes_traversees'].widget.attrs = {'data-placeholder': "2. Sélectionnez les autres communes traversées"}
        self.fields['activite'].widget.attrs = {'data-placeholder': "Choisissez d'abord une discipline"}
        self.ajouter_css_class_aux_champs_requis()

    # Validation
    def clean_date_debut(self):
        """ Valider la date de départ """
        begin_date = self.cleaned_data['date_debut']
        if not begin_date:
            raise ValidationError("La date de départ de la manifestation ne peut pas être laissée vide")
        if not hasattr(self, 'instance') or not self.instance or not self.instance.id:
            if begin_date < timezone.now():
                raise ValidationError("La date de départ de la manifestation ne peut pas être dans le passé")
        return begin_date

    def clean_ville_depart(self):
        data = self.cleaned_data['ville_depart']
        if data is None:
            raise forms.ValidationError("Ce champ doit être renseigné !")
        return data

    def clean_activite(self):
        data = self.cleaned_data['activite']
        if data is None:
            raise forms.ValidationError("Ce champ doit être renseigné !")
        return data

    def clean(self):
        cleaned_data = super().clean()
        begin_date = cleaned_data.get('date_debut')
        end_date = cleaned_data.get('date_fin')
        if begin_date and end_date and begin_date >= end_date:
            message = "La date de fin de la manifestation doit être supérieure à la date de début."
            self.add_error("date_fin", self.error_class([message]))
        if begin_date and end_date and end_date >= begin_date + timedelta(days=90):
            message = "La manifestation ne peut excéder 90 jours."
            self.add_error("date_fin", self.error_class([message]))

        # Vérifier que "departure_city" n'est pas dans la liste de "crossed_cities"
        depart = cleaned_data.get('ville_depart')
        liste = list(cleaned_data.get('villes_traversees'))
        if liste:
            cleaned_data['villes_traversees'] = [x for x in liste if x != depart]

        # Passer du champ public au champ prive
        public = cleaned_data.get('public')
        if public:
            cleaned_data['prive'] = False
        else:
            cleaned_data['prive'] = True
        return cleaned_data

    def ajouter_css_class_aux_champs_requis(self):
        """
        Ajoute une classe css nommée 'requis' à tous les champs obligatoire, c'est à dire pour que la maniefstation puisse être finalisée
        """
        for cle in self.instance.CHAMPS_REQUIS:
            field = self.fields[cle]
            css = field.widget.attrs.get('class', '')
            field.widget.attrs['class'] = 'requis ' + css

    class Meta:
        model = Manif
        exclude = ('structure', 'instance', 'charte')
        widgets = FORM_WIDGETS


class ManifSendForm(GenericForm):
    """ Formulaire d'envoi de la demande """

    # Overrides
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper.layout = Layout(HTML('<div class="alert alert-success" role="alert">'
                                         'Vous êtes sur le point d\'envoyer votre déclaration.<br>'
                                         'Après l\'envoi, son édition ne sera plus possible !<br><br>'
                                         '<strong>Les actions cochées dans la charte d\'engagement devront être réalisées.</strong><br><br>'
                                         'NB : Il est recommandé de fournir un plan de parcours de votre manifestation, '
                                         'pour cela, cliquer sur le bouton "Joindre un document"de la page précédente.'
                                         '</div>'),)
        self.helper.form_tag = False

    # Méta
    class Meta:
        model = Manif
        fields = ()


class ManifInstructeurUpdateForm(GenericForm):
    """ Formulaire destiné aux instructeurs """

    class Meta:
        model = Manif
        fields = ['afficher_adresse_structure', 'cache']

    # Overrides
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if hasattr(self, 'helper'):
            self.helper.form_tag = False


class ManifFilesForm(GenericForm):
    """ Formulaire d'upload des fichiers d'une manifestation """

    # Overrides
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper.layout = Layout(Fieldset(*FIELDS_FILES))
        self.helper.form_tag = False

    class Meta:
        model = Manif
        widgets = FORM_WIDGETS
        fields = FIELDS_FILES[1:]
