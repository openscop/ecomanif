/**
 * Created by knt on 22/01/17.
 */
$(document).ready(function () {

    // Lorsqu'une sélection est effectuée dans le champ extra_activite
    $('#id_extra_activite').bind('added', function () {
        // Récupérer la valeur du champ (le nom exact de l'activité)
        var act_name = he.decode($(this).val());

        // Récupérer l'ID de la discipline en AJAX
        $.ajax({
                   url: "/sports/ajax/discipline/by_activite_name/",
                   type: 'GET',
                   data: 'name=' + encodeURIComponent(act_name),
                   dataType: 'html',
                   success: function (data, status) {
                       // On a récupéré l'ID de la discipline sous forme de chaîne

                       // On sélectionne l'option du select de disciplines dont l'ID correspond
                       // Cela va, via le javascript de clever-selects, peupler les options de
                       // id_discipline. On va donc ensuite sélectionner la bonne option dans le
                       // champ de disciplines
                       $('#id_discipline').val(data);
                       $('#id_discipline').trigger("change");
                       $('#id_discipline').trigger("chosen:updated");  // Obligatoire avec jQuery.chosen

                       // Refaire le même type de selection sur le champ en cascade (activité)
                       setTimeout(function () {
                           $.ajax({
                                      url: "/sports/ajax/activite/by_name/",
                                      type: 'GET',
                                      data: 'name=' + act_name,
                                      dataType: 'html',
                                      success: function (data, status) {
                                          // On a récupéré l'ID de la discipline sous forme de chaîne

                                          // On sélectionne l'option du select d'activités dont l'ID correspond
                                          $('#id_activite').val(data);
                                          $('#id_activite').trigger("change");
                                          $('#id_activite').trigger("chosen:updated");  // Obligatoire avec jQuery.chosen


                                      }
                                  });
                       }, 50);

                   }
               });
        $(this).val('');

    });

    $('#id_departements_traverses').trigger("change");  // Provoquer la mise à jour du champ Communes traversées
    $('#id_departements_traverses').trigger("chosen:updated");  // Obligatoire avec jQuery.chosen
    $("#manifestation-form").liveValidate('/validate/umanifestation/', {event: 'update blur'});

    $("textarea").autogrow({flickering: false, horizontal: false});

});
