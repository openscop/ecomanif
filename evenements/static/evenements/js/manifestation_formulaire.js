/**
 * Created by knt on 22/01/17.
 */
$(document).ready(function () {

    $("textarea").autogrow({flickering: false, horizontal: false});

        // Lorsqu'une sélection est effectuée dans le champ extra_activite
    $('#id_extra_activite').bind('added', function () {
        // Récupérer la valeur du champ (le nom exact de l'activité)
        var act_name = he.decode($(this).val());

        // Récupérer l'ID de la discipline en AJAX
        $.ajax({
                   url: "/sports/ajax/discipline/by_activite_name/",
                   type: 'GET',
                   data: 'name=' + encodeURIComponent(act_name),
                   dataType: 'html',
                   success: function (data, status) {
                       // On a récupéré l'ID de la discipline sous forme de chaîne

                       // On sélectionne l'option du select de disciplines dont l'ID correspond
                       // Cela va, via le javascript de clever-selects, peupler les options de
                       // id_discipline. On va donc ensuite sélectionner la bonne option dans le
                       // champ de disciplines
                       $('#id_discipline').val(data);
                       $('#id_discipline').trigger("change");
                       $('#id_discipline').trigger("chosen:updated");  // Obligatoire avec jQuery.chosen

                       // Refaire le même type de selection sur le champ en cascade (activité)
                       setTimeout(function () {
                           $.ajax({
                                      url: "/sports/ajax/activite/by_name/",
                                      type: 'GET',
                                      data: 'name=' + act_name,
                                      dataType: 'html',
                                      success: function (data, status) {
                                          // On a récupéré l'ID de la discipline sous forme de chaîne

                                          // On sélectionne l'option du select d'activités dont l'ID correspond
                                          $('#id_activite').val(data);
                                          $('#id_activite').trigger("change");
                                          $('#id_activite').trigger("chosen:updated");  // Obligatoire avec jQuery.chosen


                                      }
                                  });
                       }, 50);

                   }
               });
        $(this).val('');

    });

    // BOF Modification pour "joindre des documents"
    // Pour les champs FileField disabled, n'afficher que le lien vers le fichier chargé
    $('.clearablefileinput').each(function () {
        if ($(this).prop("disabled")) {
            var a = $(this).parent().contents().filter("a");
            $(this).parent().html('Actuellement: ').append(a);
        }
    });
    // EOF

    // BOF Modification pour l'édition de manifestation
    // Marquer les champs de classe "requis"
    $('.requis').each(function () {
        $(this).parents('.form-group').append('<div class="col-sm-2 text-right completer"></div>');
        if ($(this).val() == '') {
            $(this).parents('.form-group').children('.completer').append('<i class="a-completer"></i> à completer</div>');
        }
    });
    AfficherIcones();
    // Gérer le marquage en fonction du remplissage
    $('.requis').on('change', function () {
        if ($(this).val() != '') {
            $(this).parents('.form-group').children('.completer').empty();
        } else {
            $(this).parents('.form-group').children('.completer').append('<i class="a-completer"></i> à completer')
        }
    AfficherIcones();
    });
    // Gérer le marquage en fonction du remplissage pour les champs 'date'
    $('.requis').on('dp.change', function () {
        if ($(this).val() != '') {
            $(this).parents('.form-group').children('.completer').empty();
        } else {
            $(this).parents('.form-group').children('.completer').append('<i class="a-completer"></i> à completer')
        }
    AfficherIcones();
    });
    // EOF


    $('#id_departements_traverses').trigger("change");  // Provoquer la mise à jour du champ Communes traversées
    $('#id_departements_traverses').trigger("chosen:updated");  // Obligatoire avec jQuery.chosen
    $("#manifestation-form").liveValidate('/validate/manifestation/', {event: 'update blur'});

});
