# coding: utf-8
import datetime
from django.utils.timezone import utc

from administrative_division.factories import DepartementFactory
from .base import EvenementsTestsBase
from notifications.models import Action
from ..factories.manif import DcnmFactory


class DcnmTests(EvenementsTestsBase):
    """ Tests Dcnm """

    def setUp(self):
        """
        La manifestation créée ici est une manifestation compétitive
        sportive non motorisée soumise à déclaration.
        """
        self.manifestation = DcnmFactory.create()
        super().setUp()

    def test_get_absolute_url(self):
        """ Tester les URL d'accès """
        manifestation = DcnmFactory.build(pk=3)
        self.assertEqual(manifestation.get_absolute_url(), '/Dcnm/3/')

    def test_display_natura2000_eval_panel(self):
        """ Vérifier que le panneau d'évaluation N2K doit s'afficher dans les cas suivants """
        self.manifestation.lucratif = True
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        self.manifestation.lucratif = False
        self.assertFalse(self.manifestation.afficher_panneau_eval_n2000())
        self.manifestation.gros_budget = True
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        self.manifestation.gros_budget = False
        self.manifestation.gros_titre = True
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())

    def test_manifestation_log_creation(self):
        action_count = Action.objects.count()
        action = Action.objects.last()
        self.assertEqual(action.user, self.manifestation.structure.organisateur.user)
        self.assertEqual(action.manif, self.manifestation.manifestation_ptr)
        self.assertEqual(action.action, "description de la manifestation")
        # Vérifier qu'aucune nouvelle action n'a été créée
        self.manifestation.save()
        self.assertEqual(action_count, Action.objects.count())

    def test_legal_delay(self):
        self.assertEqual(self.manifestation.get_delai_legal(), 60)

    def test_legal_delay_2(self):
        DepartementFactory.create()
        self.manifestation.departements_traverses.add(DepartementFactory.create())
        self.assertEqual(self.manifestation.get_delai_legal(), 90)

    def test_get_limit_date(self):
        self.manifestation.date_debut = datetime.datetime.utcnow().replace(tzinfo=utc)
        self.assertEqual(
            self.manifestation.get_date_limite(),
            (self.manifestation.date_debut - datetime.timedelta(days=60)).replace(hour=23, minute=59)
        )

    def test_delay_exceeded(self):
        self.manifestation.date_debut = datetime.datetime.utcnow().replace(tzinfo=utc) + datetime.timedelta(days=59)
        self.assertTrue(self.manifestation.delai_depasse())

    def test_delay_not_exceeded(self):
        self.manifestation.date_debut = datetime.datetime.utcnow().replace(tzinfo=utc) + datetime.timedelta(days=61)
        self.assertFalse(self.manifestation.delai_depasse())

    def test_not_two_weeks_left(self):
        self.manifestation.date_debut = datetime.datetime.today() + datetime.timedelta(days=60) +\
                                        datetime.timedelta(weeks=2)
        self.assertFalse(self.manifestation.cinq_jours_restants())

    def test_two_weeks_left(self):
        self.manifestation.date_debut = datetime.datetime.utcnow().replace(tzinfo=utc) +\
                                        datetime.timedelta(days=60) + datetime.timedelta(weeks=1)
        self.assertTrue(self.manifestation.cinq_jours_restants())
