# coding: utf-8
from django.test import TestCase


class EvenementsTestsBase(TestCase):
    """ Mixin de tests des manifestations """

    def setUp(self):
        """
        Tests : Créer une manifestation ainsi que la demande d'instruction

        Créer les services administratifs nécessaires lors de la création d'une
        manifestation sportive. La manifestation créée ici est une manifestation
        sportive compétitive non motorisée.
        """
        self.commune = self.manifestation.ville_depart
        self.departement = self.commune.get_departement()
        self.arrondissement = self.commune.get_arrondissement()
        self.instance = self.departement.get_instance()
        self.activite = self.manifestation.activite
