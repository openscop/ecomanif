# coding: utf-8

from .base import EvenementsTestsBase
from ..factories.manif import AvtmFactory


class AvtmTests(EvenementsTestsBase):
    """ Tests Avtm """

    def setUp(self):
        """
        La manifestation créée ici est une manifestation sportive motorisée.
        """
        self.manifestation = AvtmFactory.create()
        super().setUp()

    # Tests
    def test_get_absolute_url(self):
        """ Tester les URL d'accès """
        manifestation = AvtmFactory.build(pk=4)
        self.assertEqual(manifestation.get_absolute_url(), '/Avtm/4/')

    def test_display_natura2000_eval_panel(self):
        """ Vérifier que le panneau d'évaluation N2K doit s'afficher dans les cas suivants """
        self.manifestation.lucratif = True
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        self.manifestation.lucratif = False
        self.assertFalse(self.manifestation.afficher_panneau_eval_n2000())
        self.manifestation.gros_budget = True
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        self.manifestation.gros_budget = False
        self.manifestation.gros_titre = True
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        self.manifestation.gros_titre = False
        self.manifestation.vtm_natura2000 = True
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())

    def test_legal_delay(self):
        self.assertEqual(self.manifestation.get_delai_legal(), 90)
